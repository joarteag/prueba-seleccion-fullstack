const mongoose = require("mongoose");

const { Schema } = mongoose;

const Character = new Schema({
  name: String,
  slug: String,
  gender: String,
  image: String,
  pagerank: {
    title: String,
    rank: String,
  },
  house: String,
  books: [String],
  titles: [String],
});

module.exports = mongoose.model("Character", Character);
