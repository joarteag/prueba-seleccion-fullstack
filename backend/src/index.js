const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const mongoose = require("mongoose");
const routes = require("./routes");
const startUp = require("./startUp");

const url = `mongodb://localhost:27017/got`;

startUp.characters();

const app = express();

app.use(express.json());
app.use(cors());
app.use(helmet());
app.use("/characters", routes);

mongoose.connect(url, { useNewUrlParser: true });

const db = mongoose.connection;

db.on("error", (err) => {
  throw err;
});

const port = process.env.PORT || 8080;

db.once("open", () => {
  app.listen(port, () => {
    console.log(`🚀 app listening on port ${port}`);
  });
});
