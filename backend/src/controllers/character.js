const character = require("../models/character");

module.exports.getCharacter = async (req, res, next) => {
  try {
    const { id } = req.params;
    const characterFound = await character.findOne({ id });
    res.json(characterFound);
  } catch (err) {
    next(err);
  }
};

module.exports.getCharacters = async (req, res, next) => {
  try {
    const skip = parseInt(req.query.skip || 0, 10);
    const limit = parseInt(req.query.limit || 0, 10);
    const characters = await character.find({}).skip(skip).limit(limit);
    res.json(characters);
  } catch (err) {
    next(err);
  }
};
