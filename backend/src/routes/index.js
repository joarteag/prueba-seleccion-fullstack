const express = require("express");
const characterController = require("../controllers/character");

const router = express.Router();

router.get("/", characterController.getCharacters);

router.get("/:id", characterController.getCharacter);

module.exports = router;
