const axios = require("axios");
const character = require("../models/character");

const characters = () => {
  character.find({}, async (err, docs) => {
    if (err) {
      console.log(`Error: ${err}`);
    } else if (!docs.length) {
      try {
        const { data: characters } = await axios({
          method: "get",
          url: "https://api.got.show/api/book/characters",
        });
        character.collection.insertMany(characters, (error) => {
          if (error) {
            console.log(`Error : ${error.message}`);
          }
        });
      } catch (err) {
        console.log(err);
      }
    }
  });
};

module.exports = { characters };
