import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Characters, Details } from "../pages";

const Routes = () => {
  return (
    <BrowserRouter>
      <div>
        <Switch>
          <Route path="/" exact>
            <Characters />
          </Route>
          <Route path="/details" exact>
            <Details />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default Routes;
