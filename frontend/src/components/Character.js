import React from "react";
import { Card, Button } from "react-bootstrap";

export default function Character({ character, handleClick }) {
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Body>
        <Card.Title>{character.name}</Card.Title>
        <Card.Text>{character.house}</Card.Text>
        <Button
          variant="primary"
          onClick={handleClick}
        >{`More about ${character.name}`}</Button>
      </Card.Body>
    </Card>
  );
}
