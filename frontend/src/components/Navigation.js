import React from "react";
import Button from "react-bootstrap/Button";

const Navigation = ({ foward, back, skip }) => {
  return (
    <>
      <Button variant="warning" onClick={back} disabled={skip === 0}>
        Volver
      </Button>
      <Button variant="success" onClick={foward}>
        Ver otros 10
      </Button>
    </>
  );
};

export default Navigation;
