import React from "react";
import Form from "react-bootstrap/Form";

const Filter = ({ value, setValue }) => {
  return (
    <div style={{ marginBottom: "16px" }}>
      <Form.Control
        type="text"
        placeholder="Filter characters by name or house"
        onChange={(event) => setValue(event.target.value)}
        value={value}
      />
    </div>
  );
};

export default Filter;
