import React, { useState, useEffect } from "react";
import { Button, Card, Row } from "react-bootstrap";
import { useLocation, useHistory } from "react-router-dom";
import useFetch from "use-http";

const CharacterDetails = () => {
  const [data, setData] = useState(null);
  const { get, response, loading, error } = useFetch("http://localhost:8080");
  const location = useLocation();
  const history = useHistory();

  const loadCharacterDetails = async () => {
    const characters = await get(`/characters/${location.state.id}`);
    if (response.ok) {
      setData(characters);
    }
  };

  useEffect(() => {
    loadCharacterDetails();
  }, []);

  return (
    <>
      {loading && <h1>Loading...</h1>}
      {error && <h1>Error...</h1>}
      {data && (
        <Row style={{ justifyContent: "center", marginTop: "50px" }}>
          <Card style={{ width: "18rem" }}>
            {data.image && (
              <Card.Img variant="top" src={data.image} alt={data.name} />
            )}
            <Card.Body>
              <Card.Title>{data.name}</Card.Title>
              <Card.Text>Gender: {data.gender}</Card.Text>
              <Card.Text>Slug: {data.slug}</Card.Text>
              <Card.Text>Rank: {data.pagerank?.rank}</Card.Text>
              <Card.Text>House: {data.house}</Card.Text>
              <Card.Text>Books: {data.books?.join(", ")}</Card.Text>
              <Card.Text>Titles: {data.titles?.join(", ")}</Card.Text>
              <Button
                variant="primary"
                onClick={() =>
                  history.push({
                    pathname: "/",
                  })
                }
              >
                Go back
              </Button>
            </Card.Body>
          </Card>
        </Row>
      )}
    </>
  );
};

export default CharacterDetails;
