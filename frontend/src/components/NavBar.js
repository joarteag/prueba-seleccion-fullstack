import React from "react";
import Navbar from "react-bootstrap/Navbar";

const NavBar = () => {
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="/">GOT Characters</Navbar.Brand>
    </Navbar>
  );
};

export default NavBar;
