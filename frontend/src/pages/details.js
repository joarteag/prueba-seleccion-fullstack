import React from "react";
import { CharacterDetails } from "../components";

const Details = () => {
  return (
    <div>
      <CharacterDetails />
    </div>
  );
};

export default Details;
