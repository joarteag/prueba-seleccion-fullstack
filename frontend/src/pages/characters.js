import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import useFetch from "use-http";
import { generate } from "shortid";
import Row from "react-bootstrap/Row";
import { Character, Navigation, Filter } from "../components";

const Characters = () => {
  const [data, setData] = useState([]);
  const [skip, setSkip] = useState(0);
  const [filterValue, setFilterValue] = useState("");
  const { get, response, loading, error } = useFetch("http://localhost:8080");
  const limit = 10;
  const history = useHistory();

  const goFoward = () => {
    setSkip(skip + 10);
  };

  const goBack = () => {
    setSkip(skip - limit);
  };

  const loadCharacters = async () => {
    const characters = await get(`/characters?limit=${limit}&skip=${skip}`);
    if (response.ok) {
      setData(characters);
    }
  };

  const listOfCharacters = (data) => {
    return data.filter((character) => {
      const { name, house } = character;
      if (filterValue === "") {
        return true;
      } else {
        if (
          name.toLowerCase().includes(filterValue.toLowerCase()) ||
          (house !== undefined &&
            house.toLowerCase().includes(filterValue.toLowerCase()))
        ) {
          return true;
        }
      }
      return false;
    });
  };

  useEffect(() => {
    loadCharacters();
    setFilterValue("");
  }, [skip]);

  return (
    <>
      <Filter setValue={(value) => setFilterValue(value)} value={filterValue} />
      <Row style={{ justifyContent: "center" }}>
        {listOfCharacters(data).map((character) => (
          <Character
            character={character}
            key={generate()}
            handleClick={() => {
              history.push({
                pathname: "/details",
                state: { id: character.id },
              });
            }}
          />
        ))}
        {error && <h1>Error!</h1>}
        {loading && <h1>Loading</h1>}
      </Row>
      <Row style={{ justifyContent: "space-evenly", margin: "50px 0" }}>
        <Navigation foward={goFoward} back={goBack} skip={skip} />
      </Row>
    </>
  );
};

export default Characters;
