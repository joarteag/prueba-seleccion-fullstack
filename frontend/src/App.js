import React from "react";
import { NavBar } from "./components";
import Container from "react-bootstrap/Container";
import "bootstrap/dist/css/bootstrap.min.css";
import Routes from "./routes";

function App() {
  return (
    <Container>
      <NavBar />
      <Routes />
    </Container>
  );
}

export default App;
